const chai = require('chai');
const handler = require('../lib/handler');

const expect = chai.expect;
chai.should();

describe('handler test', () => {

    beforeEach(() => handler._redis = {});

    describe('dispose method', () => {

        it('basic test', () => {
            handler._intervalId = 123;
            return handler.dispose()
                .then(() => expect(handler._intervalId).to.equal(null));
        });

        it('no intervalId', () => {
            handler._intervalId = null;
            return handler.dispose()
                .then(() => expect(handler._intervalId).to.equal(null));
        });

    });

    describe('getMessage method', () => {

        it('basic test', () => {
            handler._redis.lpop = () => Promise.resolve('test');
            return handler._getMessage();
        });

        it('null message', () => {
            handler._redis.lpop = () => Promise.resolve(null);
            return handler._getMessage();
        });

        it('error handling', () => {
            handler._redis.lpop = () => Promise.reject(new Error());
            return handler._getMessage();
        })

    });

    describe('init method', () => {

        it('basic test', cb => {
            handler._redis.lpop = () => cb();
            handler.init();
        });

    });

    describe('saveError method', () => {

        it('basic test', () => {
            handler._redis.lpush = () => Promise.resolve();
            return handler._saveError('test');
        });

        it('error handling', () => {
            handler._redis.lpush = () => Promise.reject(new Error());
            return handler._saveError('test');
        });

    });

});

const chai      = require('chai');
const generator = require('../lib/generator');

const expect = chai.expect;
chai.should();

describe('generator test', () => {

    describe('dispose method', () => {

        it('basic test', () => {
            generator._intervalId           = 1;
            generator._updateOnlineStatusId = 2;
            return generator.dispose()
                .then(() => {
                    expect(generator._intervalId).to.equal(null);
                    expect(generator._updateOnlineStatusId).to.equal(null);
                });
        });

        it('not inited', () => {
            generator._intervalId           = null;
            generator._updateOnlineStatusId = null;
            return generator.dispose()
                .then(() => {
                    expect(generator._intervalId).to.equal(null);
                    expect(generator._updateOnlineStatusId).to.equal(null);
                });
        });

    });

    describe('init method', () => {

        it('basic test', () => {
            generator._redis = {
                lpush: () => Promise.resolve(),
                psetex: () => Promise.resolve()
            };
            return generator.init();
        });
    });

});

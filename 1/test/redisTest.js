require('chai').should();
const EventEmitter = require('events');
const redis        = require('../lib/redis');


describe('redis test', () => {

    beforeEach(() => redis._client = {});

    describe('dispose method', () => {

        it('basic test', cb => {
            redis._client.quit = cb;
            redis.dispose();
        });

        it('client not initialized', () => {
            redis._client = undefined;
            return redis.dispose();
        });

    });

    describe('init method', () => {

        beforeEach(() => {
            redis._redis = {
                createClient: () => new EventEmitter()
            }
        });

        it('basic test', () => {
            return redis.init();
        });

        it('error handling', () => {
            return redis.init()
                .then(() => redis._client.emit('error', new Error()));
        });

    });

    describe('atomic methods', () => {

        it('get method', () => {
            redis._client.getAsync = key => {
                key.should.equal('test');
                return Promise.resolve();
            };
            return redis.get('test');
        });

        it('lpush method', () => {
            redis._client.lpushAsync = (key, value) => {
                key.should.equal('key');
                value.should.equal('test');
                return Promise.resolve();
            };
            return redis.lpush('key', 'test');
        });

        it('lpop method', () => {
            redis._client.lpopAsync = key => {
                key.should.equal('test');
                return Promise.resolve();
            };
            return redis.lpop('test');
        });

        it('pexpire method', () => {
            redis._client.pexpireAsync = (key, ttl) => {
                key.should.equal('key');
                ttl.should.equal(60);
                return Promise.resolve();
            };
            return redis.pexpire('key', 60);
        });

        it('psetex method', () => {
            redis._client.psetexAsync = (key, ttl, value) => {
                key.should.equal('key');
                ttl.should.equal(60);
                value.should.equal('test');
                return Promise.resolve();
            };
            return redis.psetex('key', 60, 'test');
        });


        it('setnx method', () => {
            redis._client.setnxAsync = (key, value) => {
                key.should.equal('key');
                value.should.equal('test');
                return Promise.resolve();
            };
            return redis.setnx('key', 'test')
                .then(res => res.should.be.a('boolean'));
        });

    });

    describe('lpopMany', () => {

        it('basic test', () => {
            redis._client.multi = () => {
                return {
                    execAsync: () => Promise.resolve([true, false]),
                    lrange: function () {
                        return this;
                    },
                    ltrim: function () {
                        return this;
                    }
                };
            };
            return redis.lpopMany('test', 10)
                .then(res => res.should.equal(true));
        });

    });

});

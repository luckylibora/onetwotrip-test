require('chai').should();
const utils = require('../lib/utils');

describe('utils test', () => {

    describe('immediateInterval method', () => {

        it('basic test', cb => {
            let counter = 0;
            const fn    = () => {
                if (++counter === 2) {
                    cb();
                }
            };
            utils.immediateInterval(() => fn(), 1);
        });

    });

    describe('isError method', () => {

        it('basic test', () => {
            utils.isError('123').should.be.a('boolean');
        });

    });

    describe('randomString method', () => {

        it('basic test', () => {
            utils.randomString(10).should.not.equal(utils.randomString(10));
        });

    });

});
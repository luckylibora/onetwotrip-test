require('chai').should();
const errorHandler = require('../lib/errorHandler');

describe('errorHandler test', () => {

    beforeEach(() => errorHandler._redis = {});

    describe('getAllErrors method', () => {

        it('basic test', cb => {
            errorHandler._redis.lpopMany = () => Promise.resolve([1]);
            errorHandler._getAllErrors(() => cb());
        });

        it('checking errors limit', cb => {
            let firstCall                = true;
            errorHandler._redis.lpopMany = () => {
                if (firstCall) {
                    const res = [];
                    for (let i = 0; i < 1000; i++) {
                        res.push(i);
                    }
                    firstCall = false;
                    return Promise.resolve(res);
                }
                cb();
                return Promise.resolve([1]);
            };
            errorHandler._getAllErrors(() => null);
        });

    });

    describe('getErrors method', () => {

        it('basic test', () => {
            errorHandler._redis.lpopMany = () => Promise.resolve([1, 2, 3]);
            return errorHandler._getErrors()
                .then(res => res.should.deep.equal([1, 2, 3]));
        });

        it('error handling', () => {
            errorHandler._redis.lpopMany = () => Promise.reject(new Error());
            return errorHandler._getErrors();
        });

    });

    describe('init method', () => {

        it('basic test', () => {
            errorHandler._redis.lpopMany = () => Promise.resolve([1]);
            return errorHandler.init();
        });

    });

});

require('chai').should();
const onlineChecker = require('../lib/onlineChecker');

describe('onlineChecker test', () => {

    beforeEach(() => onlineChecker._redis = {});

    describe('_checkGeneratorOnline method', () => {

        it('i am new generator', cb => {
            onlineChecker._redis.setnx = () => Promise.resolve(true);
            onlineChecker.on('i_am_new_generator', () => cb());
            onlineChecker._checkGeneratorOnline();
        });

        it('generator is online', cb => {
            onlineChecker._redis.setnx = () => Promise.resolve(false);
            onlineChecker.on('i_am_new_generator', () => cb(new Error()));
            onlineChecker._checkGeneratorOnline()
                .then(() => cb());
        });

        it('error handling', () => {
            onlineChecker._redis.setnx = () => Promise.reject(new Error());
            return onlineChecker._checkGeneratorOnline();
        });

    });

    describe('init method', () => {

        it('basic test', () => {
            onlineChecker._redis = {};
            return onlineChecker.init();
        });

    });

});

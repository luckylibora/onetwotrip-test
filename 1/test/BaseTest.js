const Base = require('../lib/Base');

describe('Base test', () => {

    let base;

    beforeEach(() => base = new Base());

    describe('Method dispose', () => {

        it('basic test', () => {
            return base.dispose();
        })

    });

    describe('Method init', () => {

        it('basic test', () => {
            return base.init();
        })

    });

});

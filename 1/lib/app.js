const Base          = require('./Base');
const errorHandler  = require('./errorHandler');
const generator     = require('./generator');
const handler       = require('./handler');
const onlineChecker = require('./onlineChecker');
const redis         = require('./redis');
const utils         = require('./utils');
const winston       = require('winston');

class App extends Base {

    constructor() {
        super();
        this._errorHandler  = errorHandler;
        this._generator     = generator;
        this._handler       = handler;
        this._onlineChecker = onlineChecker;
        this._redis         = redis;
    }

    /**
     *
     * @return {Promise}
     */
    becomeGenerator() {
        return Promise.all([this._generator.init(), this._handler.dispose()])
            .catch(err => winston.error(err));
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        return this._redis.init()
            .then(() => {
                if (utils.param === 'getErrors') {
                    return this.runErrorHandler();
                }
                return this.runGeneratorHandler();
            })
            .catch(err => winston.error(err));
    }

    /**
     *
     * @return {Promise}
     */
    runErrorHandler() {
        return this._errorHandler.init()
            .catch(err => winston.error(err))
            .then(() => process.exit());
    }

    /**
     *
     * @return {Promise}
     */
    runGeneratorHandler() {
        return this._onlineChecker.init()
            .then(() => {
                this._onlineChecker.on('i_am_new_generator', () => this.becomeGenerator());
                return this._handler.init();
            });
    }

}

/**
 *
 * @type {App}
 */
module.exports = new App();

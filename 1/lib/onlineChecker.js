const Base    = require('./Base');
const config  = require('../config.json');
const redis   = require('./redis');
const utils   = require('./utils');
const winston = require('winston');

class OnlineChecker extends Base {

    constructor() {
        super();
        this._redis = redis;
    }

    /**
     *
     * @return {Promise}
     * @private
     */
    _checkGeneratorOnline() {
        return this._redis.setnx(config.generator.onlineKey, '1')
            .then(res => {
                if (res) {
                    this.emit('i_am_new_generator');
                }
            })
            .catch(err => winston.error(err));
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        utils.immediateInterval(() => this._checkGeneratorOnline(), config.generator.interval);
        return super.init();
    }

}

/**
 *
 * @type {OnlineChecker}
 */
module.exports = new OnlineChecker();

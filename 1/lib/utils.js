const crypto = require('crypto');
const os     = require('os');

/**
 *
 * @param {function()} fn
 * @param {number} delay
 * @return {Object}
 */
function immediateInterval(fn, delay) {
    setImmediate(fn);
    return setInterval(fn, delay);
}

/**
 *
 * @param {string} message
 * @return {boolean}
 */
function isError(message) {
    return Math.random() < 0.05;
}

/**
 *
 * @return {string}
 */
function param() {
    return process.argv[2];
}

/**
 *
 * @param {number = null} bytesNum
 * @return {string}
 */
function randomString(bytesNum) {
    return crypto.randomBytes(bytesNum || 8).toString('base64');
}

exports.immediateInterval = immediateInterval;

exports.isError = isError;

/**
 *
 * @type {string}
 */
exports.param = param();

exports.randomString = randomString;
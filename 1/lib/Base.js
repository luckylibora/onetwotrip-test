const EventEmitter = require('events');

class Base extends EventEmitter {

    /**
     *
     * @return {Promise}
     */
    dispose() {
        return Promise.resolve();
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        return Promise.resolve();
    }

}


module.exports = Base;
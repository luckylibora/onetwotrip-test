const Base    = require('./Base');
const config  = require('../config.json');
const crypto  = require('crypto');
const redis   = require('./redis');
const utils   = require('./utils');
const winston = require('winston');

class Generator extends Base {

    constructor() {
        super();
        this._redis = redis;
    }

    /**
     *
     * @return {Promise}
     */
    dispose() {
        if (this._intervalId) {
            clearInterval(this._intervalId);
            this._intervalId = null;
        }
        if (this._updateOnlineStatusId) {
            clearInterval(this._updateOnlineStatusId);
            this._updateOnlineStatusId = null;
        }
        return super.dispose();
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        winston.info('Generator initializing');
        winston.info('Start message generating');
        this._intervalId = utils.immediateInterval(() => this._generate(), config.messages.interval);
        winston.info('Start updating online status');
        this._updateOnlineStatusId = utils.immediateInterval(() => this._updateOnlineStatus(), config.generator.interval);
        return super.init();
    }

    /**
     *
     * @return {Promise}
     * @private
     */
    _generate() {
        const message = utils.randomString(32);
        winston.info('Message generated', message);
        return this._redis.lpush(config.messages.key, message)
            .catch(err => winston.error('Error on message writing to redis', err));
    }

    /**
     *
     * @return {Promise}
     * @private
     */
    _updateOnlineStatus() {
        return this._redis.psetex(config.generator.onlineKey, config.generator.interval * 2, '1')
            .catch(err => winston.error('Error on updating generator online status'))
    }

}

/**
 *
 * @type {Generator}
 */
module.exports = new Generator();

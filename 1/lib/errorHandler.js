const Base    = require('./Base');
const config  = require('../config.json');
const redis   = require('./redis');
const winston = require('winston');

const errorsLimit = 1000;

class ErrorHandler extends Base {

    constructor() {
        super();
        this._redis = redis;
    }

    /**
     *
     * @param {function(string)} cb
     * @return {Promise}
     * @private
     */
    _getAllErrors(cb) {
        return this._getErrors()
            .then(errors => {
                errors.forEach(err => cb(err));
                if (errors.length === errorsLimit) {
                    return this._getErrors();
                }
            });
    }

    /**
     *
     * @return {Promise.<Array.<string>>}
     * @private
     */
    _getErrors() {
        return this._redis.lpopMany(config.errors.key, errorsLimit)
            .catch(err => winston.error('Can not get errors from redis', err));
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        return this._getAllErrors(err => winston.info('Got error message', err));
    }

}

module.exports = new ErrorHandler();

const Base    = require('./Base');
const config  = require('../config.json');
const redis   = require('./redis');
const utils   = require('./utils');
const winston = require('winston');

class Handler extends Base {

    constructor() {
        super();
        this._redis = redis;
    }

    /**
     *
     * @return {Promise}
     */
    dispose() {
        if (this._intervalId) {
            clearInterval(this._intervalId);
            this._intervalId = null;
        }
        return super.dispose();
    }

    /**
     *
     * @return {Promise}
     * @private
     */
    _getMessage() {
        return this._redis.lpop(config.messages.key)
            .then(message => {
                if (message !== null) {
                    winston.info('Got message', message);
                    if (utils.isError(message)) {
                        return this._saveError(message);
                    }
                }
            })
            .catch(err => winston.error('Error on getting message', err));
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        winston.info('Handler initializing');
        this._intervalId = setInterval(() => this._getMessage(), config.messages.interval);
        return super.init();
    }

    /**
     *
     * @param {string} message
     * @return {Promise}
     * @private
     */
    _saveError(message) {
        return this._redis.lpush(config.errors.key, message)
            .catch(err => winston.error('Error on saving error message'));
    }

}

/**
 *
 * @type {Handler}
 */
module.exports = new Handler();
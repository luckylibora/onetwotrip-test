const Base     = require('./Base');
const config   = require('../config.json');
const bluebird = require('bluebird');
const redis    = require('redis');
const winston  = require('winston');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

class Redis extends Base {

    /**
     *
     * @param {Object = null} opts
     */
    constructor(opts) {
        super();
        this._opts = opts;
        this._redis = redis;
    }

    /**
     *
     * @return {Promise}
     */
    dispose() {
        if (this._client) {
            this._client.quit();
        }
        return super.dispose();
    }

    /**
     *
     * @param {string} key
     * @return {Promise.<string | null>}
     */
    get(key) {
        return this._client.getAsync(key);
    }

    /**
     *
     * @return {Promise}
     */
    init() {
        this._client = this._redis.createClient(this._opts);
        this._client.on('error', err => winston.error(err));
        return super.init();
    }

    /**
     *
     * @param {string} key
     * @param {string} value
     * @return {Promise}
     */
    lpush(key, value) {
        return this._client.lpushAsync(key, value);
    }

    /**
     *
     * @param {string} key
     * @return {Promise.<string | null>}
     */
    lpop(key) {
        return this._client.lpopAsync(key)
    }

    /**
     *
     * @param {string} key
     * @param {number} size
     * @return {Promise.<Array.<string>>}
     */
    lpopMany(key, size) {
        return this._client.multi()
            .lrange(key, 0, size - 1)
            .ltrim(key, size, -1)
            .execAsync()
            .then(res => res[0]);
    }

    /**
     *
     * @param {string} key
     * @param {number} ttl
     * @return {Promise}
     */
    pexpire(key, ttl) {
        return this._client.pexpireAsync(key, ttl);
    }

    /**
     *
     * @param {string} key
     * @param {number} ttl
     * @param {string} value
     * @return {Promise}
     */
    psetex(key, ttl, value) {
        return this._client.psetexAsync(key, ttl, value);
    }

    /**
     *
     * @param {string} key
     * @param {string} value
     * @return {Promise.<boolean>}
     */
    setnx(key, value) {
        return this._client.setnxAsync(key, value)
            .then(res => !!res);
    }

}

/**
 *
 * @type {Redis}
 */
module.exports = new Redis(config.redis);
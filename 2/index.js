function spiral(a) {
    if (!Array.isArray(a)) {
        throw new Error('Argument should be array');
    }
    const n = a.length;
    if (n % 2 === 0) {
        throw new Error('Array length should be odd');
    }
    for (let i = 0; i < n; i++) {
        const temp = a[i];
        if (!Array.isArray(temp)) {
            throw new Error('Array should be two-dimensional');
        }
        if (temp.length !== n) {
            throw new Error('Number of columns and rows should be equal');
        }
    }
    //Let's start from center
    let i     = Math.floor(n / 2);
    let j     = i;
    const res = [];
    res.push(a[i][j]);
    //circle loop
    for (let step = 2; step < n; step += 2) {
        //move to start
        j--;
        i--;
        //down
        for (let t = 0; t < step; t++) {
            res.push(a[++i][j]);
        }
        //right
        for (let t = 0; t < step; t++) {
            res.push(a[i][++j]);
        }
        //up
        for (let t = 0; t < step; t++) {
            res.push(a[--i][j]);
        }
        //left
        for (let t = 0; t < step; t++) {
            res.push(a[i][--j]);
        }
    }
    return res;
}

exports.spiral = spiral;


console.log(spiral([[11, 12, 13], [21, 22, 23], [31, 32, 33]]));

console.log(
    spiral([
        [11, 12, 13, 14, 15],
        [21, 22, 23, 24, 25],
        [31, 32, 33, 34, 35],
        [41, 42, 43, 44, 45],
        [51, 52, 53, 54, 55]
    ])
);
const chai  = require('chai');
const index = require('../');

chai.should();
const expect = chai.expect;

describe('index test', () => {

    describe('Method spiral', () => {

        it('basic test', () => {
            const res = index.spiral([
                [11, 12, 13],
                [21, 22, 23],
                [31, 32, 33]
            ]);
            res.length.should.equal(9);
            res.should.deep.equal([22, 21, 31, 32, 33, 23, 13, 12, 11]);
        });

        it('test with 5 * 5 matrix', () => {
            const res = index.spiral([
                [11, 12, 13, 14, 15],
                [21, 22, 23, 24, 25],
                [31, 32, 33, 34, 35],
                [41, 42, 43, 44, 45],
                [51, 52, 53, 54, 55]
            ]);
            res.length.should.equal(25);
            res.should.deep.equal([
                33, 32, 42, 43, 44, 34, 24, 23, 22, 21, 31, 41, 51, 52, 53, 54, 55, 45, 35, 25, 15, 14, 13, 12, 11
            ]);
        });

        it('one element matrix', () => {
            index.spiral([[1]]).should.deep.equal([1]);
        });

        it('passing not array', () => {
            expect(() => index.spiral(123)).to.throw('Argument should be array');
        });

        it('passing matrix with not odd length', () => {
            expect(() => index.spiral([[1, 2], [3, 4]])).to.throw('Array length should be odd');
        });

        it('passing one-dimension array', () => {
            expect(() => index.spiral([1, 2, 3])).to.throw('Array should be two-dimensional');
        });

        it('passing not square matrix', () => {
            expect(() => index.spiral([[1, 2], [3, 4], [5, 6]])).to.throw('Number of columns and rows should be equal');
        });

        it('passing two-dimension array with different columns number', () => {
            expect(() => index.spiral([[1, 2], [3, 4, 0], [5, 6]]))
                .to.throw('Number of columns and rows should be equal');
        });

    });

});
